﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ServiceReference1;

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        public string WebUrl = "";
      
        public ReportController(IHostingEnvironment env, IConfiguration con)
        {
            var web = con.GetSection("ServiceConnection").GetChildren().AsEnumerable();
            WebUrl = web.ToArray()[0].Value;
        }

        [HttpGet]
        [Route("GetData")]
        public ClaimSummary[] GetData()
        {
            ClaimSummary[] results;
            //ReportDataSoapClient s = new ReportDataSoapClient(new ReportDataSoapClient.EndpointConfiguration());
            EndpointAddress address = new EndpointAddress(WebUrl);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 2147483647;
            var time = new TimeSpan(0, 300, 0);
            binding.CloseTimeout = time;
            binding.OpenTimeout = time;
            binding.ReceiveTimeout = time;
            binding.SendTimeout = time;

            ReportDataSoapClient s = new ReportDataSoapClient(binding, address);
            try
            {
                /* SendEmailAsync = WebMethod in Service*/

                results = s.GetClaimsDataAsync().Result.Body.GetClaimsDataResult;
                //List<dynamic> dynamicDt = results.ToDynamic();

                /* If Service returns data it can be retrieved like below*/


            }
            catch (Exception e)
            {
                var msg = e.Message;

                throw;
            }
            return results;

        }

       

    }
    public static class DataTableExtensions
    {
        public static List<dynamic> ToDynamic(this DataTable dt)
        {
            var dynamicDt = new List<dynamic>();
            foreach (DataRow row in dt.Rows)
            {
                dynamic dyn = new ExpandoObject();
                dynamicDt.Add(dyn);
                foreach (DataColumn column in dt.Columns)
                {
                    var dic = (IDictionary<string, object>)dyn;
                    dic[column.ColumnName] = row[column];
                }
            }
            return dynamicDt;
        }
    }
}
