﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Models
/// </summary>
public class ClaimsSummary
{
    public String Hpcode = "";
    public String Opt = "";
    public string IsRiskOpt = "";
    public Decimal ServiceYear = 0;
    public Decimal ServiceMonth = 0;
    public Decimal PaidYear = 0;
    public Decimal PaidMonth = 0;
    public Decimal ReceivedYear = 0;
    public Decimal ReceivedMonth = 0;
    public Decimal Billed = 0;
    public Decimal Net = 0;
    public string Status = "";
    public string ServiceYM = "";


}