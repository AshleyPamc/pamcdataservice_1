﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;
using System.Data.SqlClient;

using System.Data;
using System.IO.Compression;
using Ionic.Zip;

/// <summary>
/// Summary description for RemittanceService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class RemittanceService : System.Web.Services.WebService
{
    System.Collections.Generic.List<FileInfo> _lResultFileList = new System.Collections.Generic.List<FileInfo>();
    public List<FileInfo> ResultFileList
    {
        get
        {
            return _lResultFileList;
        }
        set
        {
            _lResultFileList = value;
        }
    }
    private PAMC.Datatables.ClaimsTableAdapters.CLAIM_ADJUSTSTableAdapter taClmAd = new PAMC.Datatables.ClaimsTableAdapters.CLAIM_ADJUSTSTableAdapter();
    private PAMC.Datatables.ClaimsTableAdapters.ADJUST_CODESTableAdapter taAdjCd = new PAMC.Datatables.ClaimsTableAdapters.ADJUST_CODESTableAdapter();
    private PAMC.Datatables.Claims dsClm = new PAMC.Datatables.Claims();
    private PAMC.Datatables.RemittanceDataSetTableAdapters.RemittanceDataTableAdapter taERA =
        new PAMC.Datatables.RemittanceDataSetTableAdapters.RemittanceDataTableAdapter();
    private PAMC.Datatables.RemittanceDataSet dsERA = new PAMC.Datatables.RemittanceDataSet();
    private string _txtERAPath = "";
    private Int32 _buroID = 0;
    private string _provId = "";
    private int _chprefix = 0;
    private DateTime _datePaid = new DateTime();
    private string _claimno = "";
    private bool _memProv = false;
    private string _vendId;
    //public int cnt = 1;

    private bool _file = false;

    string _membidClaim = "";
    string _providClaim = "";

    public RemittanceService()
    {
        string path = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath,"login.txt");
        FileInfo f = new FileInfo(path);
            string message = "";
        string database = "";
        //bool success =  Sugoi.Security.SecurityConfiguration.LoginFromTxtFile(f,out message,out database);
        //Sugoi.Security.Rijndael.Decrypt(Sugoi.Security.SecurityConfiguration.Database);
        //Sugoi.Security.SecurityConfiguration.Database


        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public byte[] RemitancePDfDownload(int bureauId, string paidDate, string provId, bool fileType, string claimNo,int chprefix, string vendId)
    {
        byte[] testByteArray = null;
        string finalfileName = "";        

        try
        {           
            string path = Server.MapPath("~");
            DirectoryInfo di = new DirectoryInfo(path);
            //  string newFileName = $"{ di }\\assets";
            //Debug.WriteLine(newFileName);
            //if(!di.Exists)
            //{
            //    di.Create();
            //}

            //string folderName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +      // Jaco 2022-04-14 This drops leading 0's resulting in length not being the same, also not checking milliseconds 
            //                    DateTime.Now.Day.ToString() + '_' + DateTime.Now.Hour.ToString() +
            //                    DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            string folderName = DateTime.Now.ToString("yyyyMMdd_HHmmssfff"); // Jaco 2022-04-14

            DirectoryInfo newFolder = new DirectoryInfo(path + "\\assets\\" + folderName);
            newFolder.Create();
            FileInfo savedDataDir = new FileInfo(newFolder.FullName);
            DateTime date = Convert.ToDateTime(paidDate);

            _vendId = vendId; // Jaco 2022-03-24

            if ((bureauId != 0) && ((provId == null) || (provId == "")))
            {
                _datePaid = date;
                _buroID = bureauId;
                _txtERAPath = savedDataDir.FullName;
                _provId = provId;
                _file = fileType;

                StartProcessing();
                // PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, bureauId, date, provId, fileType);
                // remitanceExtractor.StartProcessing();
            }
            else
            {
                if (((provId != "") && (provId != null)) && (bureauId == 0))
                {
                    _datePaid = date;
                    _txtERAPath = savedDataDir.FullName;
                    _provId = provId;
                    _file = fileType;
                    //if (claimNo != null && claimNo != "")
                    //{
                    //    _claimno = claimNo;
                    //}
                    _chprefix = chprefix;

                    // Test - write params to file to see if they are delivered
                    //string paramsStr = "bureauId:" + bureauId + "  paidDate:" + paidDate + "  provId:" + provId + "  fileType:" + fileType + "  claimNo:" + claimNo + "  chprefix:" + chprefix + "  vendId:" + vendId;
                    //File.WriteAllText(_txtERAPath + "\\params" + cnt + ".txt", paramsStr);
                    //cnt++;

                    StartProcessing();
                    // PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, bureauId, date, provId, fileType);
                    // remitanceExtractor.StartProcessing();
                }
                else
                {
                    _txtERAPath = savedDataDir.FullName;
                    _claimno = claimNo;
                    _file = fileType;
                    StartProcessing();
                    // PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, claimNo, fileType);
                    // remitanceExtractor.StartProcessing();
                }

            }

            //  Debug.WriteLine(f.Exists.ToString());
            string day = date.Day.ToString();
            if (day.Length == 1)
            {
                day = "0" + day;
            }
            string month = date.Month.ToString();
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            string year = date.Year.ToString();
            string tempdate = year + month + day;
            if ((bureauId != 0) && ((provId == null) || (provId == "")))
            {
                if (fileType)
                {
                    finalfileName = savedDataDir.FullName + "\\PaidClaims__" + tempdate + "_Era.txt";
                }
                else
                {
                    finalfileName = savedDataDir.FullName + "\\Remits__" + tempdate + ".zip";
                }
            }
            if ((bureauId == 0) && ((provId != null) || (provId != "")))
            {
                if (fileType)
                {
                    finalfileName = savedDataDir.FullName + "\\PaidClaims_" + provId + "_" + tempdate + "_Era.txt";
                }
                else
                {
                    //finalfileName = savedDataDir.FullName + "\\Remits_" + provId + "_" + tempdate + ".zip"; // Jaco 2024-04-04 for provider remittance file must not be zipped 
                    finalfileName = savedDataDir.FullName + "\\Rem_" + chprefix + "_" + vendId + ".pdf";   // Jaco 2024-04-04 for provider remittance file must not be zipped 
                }
            }

            if ((bureauId == 0) && ((provId == null) || (provId == "")))
            {
                if (fileType)
                {
                    finalfileName = savedDataDir.FullName + "\\PaidClaims__00010101_Era.txt";
                }
                else
                {
                    finalfileName = savedDataDir.FullName + "\\Rem_" + claimNo + ".zip";
                }
            }

            FileInfo newFile = new FileInfo(finalfileName);
            testByteArray = File.ReadAllBytes(newFile.FullName);
            //newFile.Delete(); //Jaco 2022-03-24 folder becomes too full
            Debug.WriteLine(testByteArray.Length);
        }
        catch (Exception e)
        {

            //File.WriteAllText(_txtERAPath + "\\error1.txt", e.Message + "\r\n\r\n\r\n" + e.StackTrace);

            Debug.WriteLine(e.StackTrace);
            throw;
        }
        Debug.Write(testByteArray);
        return testByteArray;
    }

    [WebMethod]
    public byte[] RemitanceClientClaimDownload(bool fileType, string claimNo, bool membProv)
    {
        byte[] returnedArray;

        string finalfileName = "";

        try
        {
            string path = Server.MapPath("~");
            DirectoryInfo di = new DirectoryInfo(path);

            //string folderName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +      // Jaco 2022-04-14 This drops leading 0's resulting in length not being the same, also not checking milliseconds 
            //                    DateTime.Now.Day.ToString() + '_' + DateTime.Now.Hour.ToString() +
            //                    DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            string folderName = DateTime.Now.ToString("yyyyMMdd_HHmmssfff"); // Jaco 2022-04-14

            DirectoryInfo newFolder = new DirectoryInfo(path + "\\assets\\" + folderName);
            newFolder.Create();
            FileInfo savedDataDir = new FileInfo(newFolder.FullName);


            _txtERAPath = savedDataDir.FullName;
            _memProv = membProv;
            _claimno = claimNo;
            _file = fileType;

            StartProcessing();
            //PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, claimNo, membProv, fileType);
            //  remitanceExtractor.StartProcessing();

            if (fileType == false)
            {
                //finalfileName = savedDataDir.FullName + "\\Rem_" + claimNo + ".pdf";
                finalfileName = savedDataDir.FullName + "\\Rem_" + _providClaim + "_" + _membidClaim + ".pdf";
            }
            else
            {
                finalfileName = savedDataDir.FullName + "\\PaidClaims__00010101_Era.txt";
            }


            FileInfo newFile = new FileInfo(finalfileName);
            returnedArray = File.ReadAllBytes(newFile.FullName);
            //newFile.Delete(); //Jaco 2022-03-24 folder becomes too full
            Debug.WriteLine(returnedArray.Length);
        }
        catch (Exception e)
        {
            //File.WriteAllText(_txtERAPath + "\\error2.txt", e.Message + "\rn\rn\rn" + e.StackTrace);
            
            var msg = e.Message;
            Debug.WriteLine(e.StackTrace);
            throw;
        }

        return returnedArray;
    }

    public void StartProcessing()
    {
        //if (Sugoi.Security.SecurityConfiguration.Connections == null)
        {
            taERA.Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DRCConnectioString2"].ConnectionString);
        }
        ArrayList _arr = new ArrayList();
        ERASet era = new ERASet();
        // taERA.SetTimeOut(600);
        if (_claimno != "")
        {
            try
            {
                taERA.FillByClaimno(dsERA.RemittanceData, _claimno);
            }
            catch (Exception x)
            {
                //File.WriteAllText(_txtERAPath + "\\error3.txt", x.Message + "\r\n\r\n\r\n" + x.StackTrace);
                Exception ex = new Exception("An Error Occured", x);

                throw ex;
            }
        }
        else if (_buroID != 0)
        {
            //Int32 cntUpdate = taERA.UpdateQuery(_buroEmail, _datePaid);

            //if (cntUpdate > 0)
            //{
            try
            {
                taERA.Fill(dsERA.RemittanceData, _buroID, _datePaid);
            }
            catch (Exception ex)
            {
                //File.WriteAllText(_txtERAPath + "\\error4.txt", ex.Message + "\r\n\r\n\r\n" + ex.StackTrace);
                if (System.Environment.UserInteractive)
                {
                    //MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                }
            }
            //}
        }
        else
        {
            if (_vendId != _provId)
            {
                taERA.FillByVendorProviderDatePaidChprefix(dsERA.RemittanceData, _vendId, _datePaid, _provId, _chprefix);
            }
            else
            {
                taERA.FillByVendorDatePaidChprefix(dsERA.RemittanceData, _provId, _datePaid, _chprefix);
            }
        }
        if (_file)
        {

            RetriveFiles(_arr, era);
        }
        else
        {

            GenerateRemittances(_arr, era, "Data Source=192.168.16.13;Initial Catalog=DRC;User ID=PamcPortal;Password=#Portal@Pamc", "Data Source=192.168.16.13;Initial Catalog=Reporting;User ID=PamcPortal;Password=#Portal@Pamc");
        }

    }

    private void RetriveFiles(ArrayList _arr, ERASet era)
    {
        if (dsERA.RemittanceData.Count > 0)
        {

            ERASet.eRaFileHeader h = new ERASet.eRaFileHeader();

            h.BATCH = DateTime.Now.ToString("yyyyMMdd");

            h.CTEATEDATE = DateTime.Now.ToString("yyyyMMdd");
            h.SWITCH = _buroID.ToString();
            _arr.Add(h);
            Decimal dFileTotal = 0;

            string prevVENDOR = "";
            string prevDatePaid = "";
            Decimal dProvTotal = 0;
            Int64 cntProv = 0;

            foreach (PAMC.Datatables.RemittanceDataSet.RemittanceDataRow rERA in dsERA.RemittanceData)
            {
                try
                {
                    if (rERA.VENDOR != prevVENDOR | rERA.DATEPAID.ToString("yyyyMMdd") != prevDatePaid)
                    {
                        if (prevVENDOR != "")
                        {
                            ERASet.eRaProviderFooter epf = new ERASet.eRaProviderFooter();
                            epf.PROVIDERLINES = cntProv.ToString();
                            epf.PROVIDERTOTAL = dProvTotal.ToString("f");
                            _arr.Add(epf);
                            dProvTotal = 0;
                            cntProv = 0;
                        }
                        ERASet.eRaProviderHeader ph = new ERASet.eRaProviderHeader();
                        //ph.ACCTNO = rERA.ACCNR;
                        //ph.ACCTTYPE = rERA.ACCTYPE;
                        //ph.BANKNAME = rERA.BANK;
                        //ph.BRANCHNUMBER = rERA.BRANCHCODE;
                        ph.DATEPAID = rERA.DATEPAID.ToString("yyyyMMdd");
                        ph.PROVID = rERA.VENDOR;

                        _arr.Add(ph);
                    }

                    try
                    {
                        ERASet.eRaClaimDetail d = new ERASet.eRaClaimDetail();
                        if (rERA.ADJCODEWH.Trim().Length > 0)
                        {
                            d.ADJCODE = rERA.ADJCODEWH.Trim();
                            d.ADJUST = rERA.ADJUSTWH.ToString("f");
                            d.ADJDESC = rERA.WHDESC;
                        }
                        else
                        {
                            d.ADJCODE = rERA.ADJCODE.Trim();
                            d.ADJUST = rERA.ADJUST.ToString("f");
                            d.ADJDESC = rERA.ADJDESC;
                        }
                        if (d.ADJCODE.Trim() == "##")
                        {
                            taClmAd.Fill(dsClm.CLAIM_ADJUSTS, rERA.CLAIMNO, rERA.TBLROWID);
                            int cntAdj = 0;
                            foreach (PAMC.Datatables.Claims.CLAIM_ADJUSTSRow rCA in dsClm.CLAIM_ADJUSTS.Rows)
                            {
                                cntAdj++;
                                taAdjCd.Fill(dsClm.ADJUST_CODES, rCA.ADJCODE);
                                if (cntAdj == 1)
                                {
                                    d.ADJCODE = rCA.ADJCODE.Trim();
                                    d.ADJDESC = dsClm.ADJUST_CODES[0].DESCR.Trim();
                                }
                                else
                                {
                                    if (!d.ADJCODE.Contains(rCA.ADJCODE.Trim()))
                                    {
                                        d.ADJCODE = d.ADJCODE + ';' + rCA.ADJCODE.Trim();
                                        d.ADJDESC = d.ADJDESC + ';' + dsClm.ADJUST_CODES[0].DESCR.Trim();
                                    }
                                }
                            }
                        }
                        d.BILLED = rERA.BILLED.ToString("f");
                        d.CLAIMNO = rERA.CLAIMNO;
                        d.CONTRACTVALUE = rERA.SCHEMETARRIF.ToString("f");
                        d.COPAY = rERA.COPAY.ToString("f");
                        d.DIAGCODE = rERA.DIAGCODE;
                        d.FROMDATESVC = rERA.FROMDATESVC.ToString("yyyyMMdd");
                        d.MEMBERBIRTH = rERA.BIRTH.ToString("yyyyMMdd");

                        d.MEMBID = rERA.MEMBID;
                        //if (_buroEmail == "RECON")
                        //{
                        //    d.MEMBNAME = rERA.HPCODE;
                        //}

                        d.NAPPI = rERA.PROCCODE.Trim();
                        d.NET = rERA.NET.ToString("f");

                        if (rERA.PROVCLAIM.Contains("/"))
                        {
                            d.PROVCLAIM = rERA.PROVCLAIM.Substring(rERA.PROVCLAIM.IndexOf('/') + 1);

                            if (d.PROVCLAIM.Contains("/"))
                            {
                                d.PROVCLAIM = d.PROVCLAIM.Substring(0, d.PROVCLAIM.LastIndexOf('/'));
                                d.IPBATCH = d.PROVCLAIM.Substring(d.PROVCLAIM.LastIndexOf('/') + 1);
                            }
                            else
                            {
                                d.PROVCLAIM = rERA.PROVCLAIM;
                            }
                        }
                        else
                        {
                            d.PROVCLAIM = rERA.PROVCLAIM;
                            //if (!rERA.IsBATCH_NONull())
                            //{
                            //    d.IPBATCH = rERA.BATCH_NO;
                            //}
                            if (!rERA.IsCHPREFIXNull())
                            {
                                d.IPBATCH = rERA.CHPREFIX.ToString();
                            }
                        }

                        dProvTotal += rERA.NET;
                        dFileTotal += rERA.NET;
                        cntProv++;
                        _arr.Add(d);

                        prevVENDOR = rERA.VENDOR;
                        prevDatePaid = rERA.DATEPAID.ToString("yyyyMMdd");

                    }
                    catch (Exception ex)
                    {
                        //File.WriteAllText(_txtERAPath + "\\error5.txt", ex.Message + "\r\n\r\n\r\n" + ex.StackTrace);
                        if (System.Environment.UserInteractive)
                        {
                            // MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                        }
                        //ClsEventLog._eLog.WriteToEventLog("E", "PaidClaimsExport", "An error occurred: \r\nMessage: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace);
                    }

                }
                catch (Exception ex)
                {
                    //File.WriteAllText(_txtERAPath + "\\error6.txt", ex.Message + "\r\n\r\n\r\n" + ex.StackTrace);
                    if (System.Environment.UserInteractive)
                    {
                        //MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                    }
                    //ClsEventLog._eLog.WriteToEventLog("E", "PaidClaimsExport", "An error occurred: \r\nMessage: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace);
                }
            }
            ERASet.eRaProviderFooter pf = new ERASet.eRaProviderFooter();
            pf.PROVIDERLINES = cntProv.ToString();
            pf.PROVIDERTOTAL = dProvTotal.ToString("f");
            _arr.Add(pf);
            ERASet.eRaFileFooter f = new ERASet.eRaFileFooter();
            f.COUNT = dsERA.RemittanceData.Count.ToString();
            f.FILETOTALNET = dFileTotal.ToString("f");
            _arr.Add(f);

            string fileName = _txtERAPath + "\\PaidClaims_" + _provId + "_" + _datePaid.ToString("yyyyMMdd") + "_Era.txt";

            era.WriteeRaSet(fileName, _arr.ToArray());
            ResultFileList.Add(new FileInfo(fileName));
            if (System.Environment.UserInteractive)
            {
                //MessageBox.Show("Done!");
            }
            //ClsEventLog._eLog.WriteToEventLog("S", "PaidClaimsExport", "File exported successfully!");
        }
        else
        {
            if (System.Environment.UserInteractive)
            {
                //ClsEventLog._eLog.WriteToEventLog("W", "PaidClaimsExport", "No Paid Claims for the past month!");
            }
        }
    }

    private void GenerateRemittances(ArrayList _arr, ERASet era, string strConnectstring, string strRepConnString)
    {
        if (dsERA.RemittanceData.Count > 0)
        {
            Decimal dFileTotal = 0;

            string prevVendID = "";
            string prevDatePaid = "";
            Decimal dProvTotal = 0;
            Int64 cntProv = 0;
            
            SqlConnection cnRep = new SqlConnection(strRepConnString);
            cnRep.Open();
                        
            PAMC.Remittances.ThirdPartyBatchRemittances.EzcapRemittanceGenerator cERG = new PAMC.Remittances.ThirdPartyBatchRemittances.EzcapRemittanceGenerator(strConnectstring, strRepConnString, _txtERAPath);
            //GalaxyNet.EZCAP.Controllers.EzcapRemittanceGenerater cERG = new GalaxyNet.EZCAP.Controllers.EzcapRemittanceGenerater(strConnectstring, strRepConnString);
            ArrayList al = new ArrayList();
            foreach (PAMC.Datatables.RemittanceDataSet.RemittanceDataRow rERA in dsERA.RemittanceData)
            {
                try
                {

                    if (rERA.VENDOR != prevVendID | rERA.DATEPAID.ToString("yyyyMMdd") != prevDatePaid)
                    {
                        PAMC.Datatables.RemittanceDataSet dsR = new PAMC.Datatables.RemittanceDataSet();
                        PAMC.Datatables.RemittanceDataSetTableAdapters.CHECK_RUN_DOCSTableAdapter taCRD = new PAMC.Datatables.RemittanceDataSetTableAdapters.CHECK_RUN_DOCSTableAdapter();
                        taCRD.Connection = new System.Data.SqlClient.SqlConnection(strRepConnString);

                        taCRD.FillByPrefixCheckno(dsR.CHECK_RUN_DOCS, rERA.CHPREFIX, rERA.CHECKNO);

                        if (dsR.CHECK_RUN_DOCS.Count > 0)
                        {
                            //int deleted = GalaxyNet.Reporting.Tools.RepTools.DeleteRemittancesFromCheckRunDocs(cnRep, rERA.CHPREFIX.ToString(), rERA.CHECKNO.ToString());

                            //SqlConnection cnRep = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ReportingConnectionString"].ConnectionString);
                                                       
                            if (cnRep.State != ConnectionState.Open)
                            {
                                cnRep.Open();
                            }

                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = cnRep;
                            cmd.CommandTimeout = 60000;

                            cmd.Parameters.Add(new SqlParameter("@CHPREFIX", dsR.CHECK_RUN_DOCS[0].CHPREFIX));
                            cmd.Parameters.Add(new SqlParameter("@CHECKNO", dsR.CHECK_RUN_DOCS[0].CHECKNO));
                            cmd.CommandText = "DELETE FROM [dbo].[CHECK_RUN_DOCS] WHERE " +
                            "CHPREFIX = @CHPREFIX  AND CHECKNO = @CHECKNO";

                            int deleted = cmd.ExecuteNonQuery();

                        }
                        int dummy1 = 0;
                        int dummy2 = 0;
                        string claimType = "P";
                        if (_claimno != "")
                        {
                            claimType = rERA.PAIDTO;
                        }
                        string email = "";
                        if (!rERA.IsEMAILNull())
                        {
                            email = rERA.EMAIL;
                        }
                        Byte[] arPdfFile = null;
                        if (_memProv)
                        {
                           
                            arPdfFile = cERG.CreateCheckRunDoc(ref dummy1, ref dummy2, rERA.CHPREFIX, rERA.CHECKNO, rERA.DATEPAID, email, rERA.VENDOR, claimType, false, rERA.SUBSSN, "E", "", "", rERA.HPCODE, true);

                        }
                        else
                        {
                            arPdfFile = cERG.CreateCheckRunDoc(ref dummy1, ref dummy2, rERA.CHPREFIX, rERA.CHECKNO, rERA.DATEPAID, email, rERA.VENDOR, claimType, true, "", "E", "", "", rERA.HPCODE, false);
                        }

                        //   taCRD.FillByPrefixCheckno(dsR.CHECK_RUN_DOCS, rERA.CHPREFIX, rERA.CHECKNO);
                        string nameR = "";
                        if (_claimno != "")
                        {
                            _membidClaim = rERA.MEMBID.ToString().Trim();
                            _providClaim = rERA.PROVID.ToString();
                            nameR = _txtERAPath + @"\Rem_" + rERA.PROVID.ToString() + "_" + rERA.MEMBID.ToString().Trim() + ".pdf";
                        }
                        else
                        {
                            nameR = _txtERAPath + @"\Rem_" + rERA.CHPREFIX.ToString() + "_" + rERA.VENDOR.ToString().Trim() + ".pdf";
                        }

                        PAMC.Remittances.ThirdPartyBatchRemittances.EzcapRemittanceGenerator eRcg = new PAMC.Remittances.ThirdPartyBatchRemittances.EzcapRemittanceGenerator(strConnectstring, strRepConnString, _txtERAPath);
                                                
                        FileInfo fileRem = eRcg.GetFile(arPdfFile, nameR);
                        al.Add(fileRem);
                        //}
                        //else
                        //{
                        //    Byte[] arPdfFile = dsR.CHECK_RUN_DOCS[0].PAYRUNDOC;
                        //    string nameR = _txtERAPath + @"\\Rem_" + rERA.CHPREFIX.ToString() + "_" + rERA.VENDOR.ToString().Trim() + ".pdf";
                        //    FileInfo fileRem = GalaxyNet.Reporting.Controllers.PDFRemittance.GetFile(arPdfFile, nameR);
                        //    al.Add(fileRem);
                        //}

                        //era.WriteeRaSet(name, _arr.ToArray());
                        //ResultFileList.Add(new FileInfo(fileName));
                    }
                    prevVendID = rERA.VENDOR;
                    prevDatePaid = rERA.DATEPAID.ToString("yyyyMMdd");
                }
                catch (Exception ex)
                {
                    //File.WriteAllText(_txtERAPath + "\\error7.txt", ex.Message + "\r\n\r\n\r\n" + ex.StackTrace);
                    if (System.Environment.UserInteractive)
                    {
                        //MessageBox.Show("An error occurred : \r\n" + ex.Message + "\r\n" + ex.StackTrace);
                    }
                    //ClsEventLog._eLog.WriteToEventLog("E", "PaidClaimsExport", "An error occurred: \r\nMessage: " + ex.Message + "\r\nStackTrace: " + ex.StackTrace);
                }
            }


            DirectoryInfo dr = new DirectoryInfo(_txtERAPath);
            foreach (FileInfo item in dr.GetFiles())
            {
                if (item.Extension.ToUpper().Contains("RPT") || item.Extension.ToUpper().Contains("ZIP"))
                {
                    item.Delete();
                }
            }

            string fileName = "";            
            if (_claimno != "" && ( _provId == null || _provId == "") && (_buroID == null || _buroID == 0))
            {
                //fileName = _txtERAPath + @"\Rem_" + _claimno;
                fileName = _txtERAPath + @"\Rem_" + _providClaim + "_" + _membidClaim;
            }
            else if (_provId != null && _provId != "" && (_buroID == null || _buroID == 0))
            {
                fileName = _txtERAPath + "\\Rem_" + _chprefix + "_" + _vendId;
            }
            else if (_buroID != null && _buroID != 0)
            {
                fileName = _txtERAPath + "\\Remits_" + _vendId + "_" + _datePaid.ToString("yyyyMMdd");
            }



            //FileInfo fileToZip = new FileInfo(fileName);
            FileInfo fileToProcess = new FileInfo(fileName);

            //string zippedfile = ZipFileInDirectory(fileToZip.FullName, "", true, _txtERAPath);
            string processedFile = "";

            if (_buroID == null || _buroID == 0)
            {                
                processedFile = fileToProcess.FullName + ".pdf";
            }
            else
            {
                processedFile = ZipFileInDirectory(fileToProcess.FullName, "", true, _txtERAPath);
            }
        

            //C:\PAMC_WEB\pamcdataservice_1\PAMCDataService\PAMC_Data\assets\20240404_152807584\Remits_0006203_20240322.zip



            //era.WriteeRaSet(fileName, _arr.ToArray());
            //ResultFileList.Add(new FileInfo(zippedfile));
            ResultFileList.Add(new FileInfo(processedFile));  
            if (System.Environment.UserInteractive)
            {
                //MessageBox.Show("Done!");
            }
            //ClsEventLog._eLog.WriteToEventLog("S", "PaidClaimsExport", "File exported successfully!");
        }
        else
        {
            if (System.Environment.UserInteractive)
            {
                //ClsEventLog._eLog.WriteToEventLog("W", "PaidClaimsExport", "No Paid Claims for the past month!");
            }
        }
    }

    public static string ZipFileInDirectory(string fileName, string outputDirectory, bool allInDirectory, string _txtERAPath)
    {
        string zipFilename = "";
        try
        {
            using (ZipFile zip = new ZipFile())
            {
                FileInfo fo = new FileInfo(fileName);
                if (allInDirectory)
                {
                    if (fileName.Contains(".dll"))
                    {
                        FileInfo[] foD = fo.Directory.GetFiles();
                        foreach (FileInfo fi in foD)
                        {
                            zip.AddFile(fi.FullName, "");// zip.AddFile(fi.Name);
                        }
                        zipFilename = fo.DirectoryName + "\\AllFiles.zip";
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(fo.FullName);
                        foreach (FileInfo fi in fo.Directory.GetFiles())
                        {
                            zip.AddFile(fi.FullName, "");
                        }
                        zipFilename = fileName + ".zip";
                    }
                    // FileInfo    
                }
                else
                {
                    zip.AddFile(fileName, "");
                    zipFilename = fo.DirectoryName + @"\" + fo.Name.Substring(0, fo.Name.IndexOf(fo.Extension)) + ".zip";
                }

                try
                {
                    zip.Save(zipFilename);
                    if (allInDirectory)
                    {
                        FileInfo[] foD = fo.Directory.GetFiles();
                        foreach (FileInfo fi in foD)
                        {
                            if (fi.Extension != ".zip")
                            {
                                fi.Delete();
                            }
                        }
                    }
                    else
                    {
                        if (fo.Exists)
                        {
                            fo.Delete();
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    //File.WriteAllText(_txtERAPath + "\\error8.txt", ex.Message + "\r\n\r\n\r\n" + ex.StackTrace);
                    throw;
                }
                catch (Exception ex1)
                {
                    //File.WriteAllText(_txtERAPath + "\\error9.txt", ex1.Message + "\r\n\r\n\r\n" + ex1.StackTrace);
                }

                fo = null;
            }
        }
        catch (Exception ex)
        {
            //File.WriteAllText(_txtERAPath + "\\error10.txt", ex.Message + "\r\n\r\n\r\n" + ex.StackTrace);
            Debug.WriteLine("ZIP failed for " + fileName + " - Error : " + ex.Message + "\r\nStackTrace: {ex.StackTrace}");
            throw;
        }
        return zipFilename;

    }

}
