﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ERASet
/// </summary>
public class ERASet
{
    [IgnoreEmptyLines()]
    [DelimitedRecord("|")]
    public class eRaFileHeader
    {
        [FieldTrim(TrimMode.Both, " 	")]
        private String mTYPE = "0";
        public String TYPE
        {
            get { return mTYPE; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mBATCH;
        public String BATCH
        {
            get { return mBATCH; }
            set { mBATCH = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mSWITCH;
        public String SWITCH
        {
            get { return mSWITCH; }
            set { mSWITCH = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mUPDATETYPE = "ERA";
        public String UPDATETYPE
        {
            get { return mUPDATETYPE; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mCTEATEDATE;
        public String CTEATEDATE
        {
            get { return mCTEATEDATE; }
            set { mCTEATEDATE = value; }
        }

        string dummy = "";
    }

    [IgnoreEmptyLines()]
    [DelimitedRecord("|")]
    public class eRaProviderHeader
    {
        [FieldTrim(TrimMode.Both, " 	")]
        private String mTYPE = "1";
        public String TYPE
        {
            get { return mTYPE; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mPROVID;
        public String PROVID
        {
            get { return mPROVID; }
            set { mPROVID = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mDATEPAID;
        public String DATEPAID
        {
            get { return mDATEPAID; }
            set { mDATEPAID = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mPAYMENTTYPE = "1";
        public String PAYMENTTYPE
        {
            get { return mPAYMENTTYPE; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mBANKNAME;
        public String BANKNAME
        {
            get { return mBANKNAME; }
            set { mBANKNAME = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mBRANCHNUMBER;
        public String BRANCHNUMBER
        {
            get { return mBRANCHNUMBER; }
            set { mBRANCHNUMBER = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mACCTNO;
        public String ACCTNO
        {
            get { return mACCTNO; }
            set { mACCTNO = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mACCTTYPE;
        public String ACCTTYPE
        {
            get { return mACCTTYPE; }
            set { mACCTTYPE = value; }
        }
        string dummy = "";
    }

    [IgnoreEmptyLines()]
    [DelimitedRecord("|")]
    public class eRaClaimDetail
    {
        [FieldTrim(TrimMode.Both, " 	")]
        private String mTYPE = "2";
        public String TYPE
        {
            get { return mTYPE; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mIPBATCH;
        public String IPBATCH
        {
            get { return mIPBATCH; }
            set { mIPBATCH = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mPROVCLAIM;
        public String PROVCLAIM
        {
            get { return mPROVCLAIM; }
            set { mPROVCLAIM = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mMEMBID;
        public String MEMBID
        {
            get { return mMEMBID; }
            set { mMEMBID = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mMEMBNAME;
        public String MEMBNAME
        {
            get { return mMEMBNAME; }
            set { mMEMBNAME = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mMEMBERBIRTH = "1";
        public String MEMBERBIRTH
        {
            get { return mMEMBERBIRTH; }
            set { mMEMBERBIRTH = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mCLAIMNO;
        public String CLAIMNO
        {
            get { return mCLAIMNO; }
            set { mCLAIMNO = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mFROMDATESVC;
        public String FROMDATESVC
        {
            get { return mFROMDATESVC; }
            set { mFROMDATESVC = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mDIAGCODE;
        public String DIAGCODE
        {
            get { return mDIAGCODE; }
            set { mDIAGCODE = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mNAPPI;
        public String NAPPI
        {
            get { return mNAPPI; }
            set { mNAPPI = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mBILLED;
        public String BILLED
        {
            get { return mBILLED; }
            set { mBILLED = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mCONTRACTVALUE;
        public String CONTRACTVALUE
        {
            get { return mCONTRACTVALUE; }
            set { mCONTRACTVALUE = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mCOPAY;
        public String COPAY
        {
            get { return mCOPAY; }
            set { mCOPAY = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mNET;
        public String NET
        {
            get { return mNET; }
            set { mNET = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mADJUST;
        public String ADJUST
        {
            get { return mADJUST; }
            set { mADJUST = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mADJCODE;
        public String ADJCODE
        {
            get { return mADJCODE; }
            set { mADJCODE = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mADJDESC;
        public String ADJDESC
        {
            get { return mADJDESC; }
            set { mADJDESC = value; }
        }
        string dummy = "";
    }

    [DelimitedRecord("|")]
    public class eRaProviderFooter
    {
        [FieldTrim(TrimMode.Both, " 	")]
        private String mTYPE = "5";
        public String TYPE
        {
            get { return mTYPE; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mPROVIDERTOTAL;
        public String PROVIDERTOTAL
        {
            get { return mPROVIDERTOTAL; }
            set { mPROVIDERTOTAL = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mPROVIDERLINES;
        public String PROVIDERLINES
        {
            get { return mPROVIDERLINES; }
            set { mPROVIDERLINES = value; }
        }
        string dummy = "";

    }

    [DelimitedRecord("|")]
    public class eRaFileFooter
    {
        [FieldTrim(TrimMode.Both, " 	")]
        private String mTYPE = "9";
        public String TYPE
        {
            get { return mTYPE; }
            set { mTYPE = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mFILETOTALNET;
        public String FILETOTALNET
        {
            get { return mFILETOTALNET; }
            set { mFILETOTALNET = value; }
        }

        [FieldTrim(TrimMode.Both, " 	")]
        private String mCOUNT;
        public String COUNT
        {
            get { return mCOUNT; }
            set { mCOUNT = value; }
        }

        string dummy = "";
    }

    private object[] mSet;
    public object[] Set
    {
        get { return mSet; }
        set { mSet = value; }
    }
    public void WriteeRaSet(string strPath, object[] set)
    {
        try
        {
            MultiRecordEngine engine;
            engine = new MultiRecordEngine(
                         new RecordTypeSelector(eRaSelector), new Type[] {
                            typeof(eRaProviderHeader), typeof(eRaFileHeader), typeof(eRaClaimDetail)
                , typeof(eRaProviderFooter), typeof(eRaFileFooter)});

            //TextFile fileWriter = new TextFile();
            //fileWriter.FileHelperWriter(strPath, set, engine);
            engine.WriteFile(strPath, set);
        }
        catch { }

    }

    Type eRaSelector(MultiRecordEngine engine, string record)
    {
        if (record[0].ToString() == "0")
            return typeof(eRaFileHeader);
        else if (record[0].ToString() == "1")
            return typeof(eRaProviderHeader);
        else if (record[0].ToString() == "2")
            return typeof(eRaClaimDetail);
        else if (record[0].ToString() == "5")
            return typeof(eRaProviderFooter);
        else
            return typeof(eRaFileFooter);
    }

    public class eRaSet
    {
        private object[] mSet;
        public object[] Set
        {
            get { return mSet; }
            set { mSet = value; }
        }

        public eRaSet()
        {
        }

        public void WriteeRaSet(string strPath, object[] set)
        {
            try
            {
                MultiRecordEngine engine;
                engine = new MultiRecordEngine(
                             new RecordTypeSelector(eRaSelector), new Type[] {
                            typeof(eRaProviderHeader), typeof(eRaFileHeader), typeof(eRaClaimDetail)
                , typeof(eRaProviderFooter), typeof(eRaFileFooter)});

                //TextFile fileWriter = new TextFile();
                //fileWriter.FileHelperWriter(strPath, set, engine);
                engine.WriteFile(strPath, set);
            }
            catch { }

        }

        Type eRaSelector(MultiRecordEngine engine, string record)
        {
            if (record[0].ToString() == "0")
                return typeof(eRaFileHeader);
            else if (record[0].ToString() == "1")
                return typeof(eRaProviderHeader);
            else if (record[0].ToString() == "2")
                return typeof(eRaClaimDetail);
            else if (record[0].ToString() == "5")
                return typeof(eRaProviderFooter);
            else
                return typeof(eRaFileFooter);
        }

    }
}