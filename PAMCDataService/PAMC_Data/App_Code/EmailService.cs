﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for EmailService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EmailService : System.Web.Services.WebService
{

    public EmailService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public bool SendEmail(string body, string subject, string toAddress)
    {
        bool sended = false;
        string[] emailList;

        try
        {
            /*emailList = new string[toAddress.Split(';').Length];
            emailList = toAddress.Split(';');

            using (var message = new MailMessage())
            {
                
                foreach (string addr in emailList)
                {
                    message.To.Add(new MailAddress(addr));
                }
                //message.To.Add(new MailAddress(toAddress));
              //  message.From = new MailAddress("ServiceRequest@pamc.co.za");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = false;

                using (var client = new SmtpClient("192.168.16.21"))
                {

                    client.UseDefaultCredentials = false;
                    //client.Port = 465;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = new NetworkCredential("ServiceRequest", "Serv12#$");
                    message.From = new MailAddress("ServiceRequest@pamc.co.za");
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    client.EnableSsl = true;
                    client.Send(message);
                    sended = true;
                }
            }*/

            using(SqlConnection  cn = new SqlConnection(
                System.Configuration.ConfigurationManager.ConnectionStrings["DRCConnectioString2"].ConnectionString))
            {
                if(cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '3'";
                dt.Load(cmd.ExecuteReader());

                string server = "";
                string username = "";
                string password = "";
                string from = "";
                string port = "";

                foreach (DataRow row in dt.Rows)
                {
                    server = row["SERVERADDRESS"].ToString();
                    password = row["PASSWORD"].ToString();
                    username = row["USERNAME"].ToString();
                    from = row["MAILFROM"].ToString();
                    port = row["OUTGOINGPORT"].ToString();
                }

                List<FileInfo> attachments = new List<FileInfo>();
                sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, toAddress, attachments);


            }

            //sended = PAMC.EmailSender.Sender.SendEmailViaSMTP("","","","","","","",null);

        }
        catch (Exception e)
        {
            var msg = e.Message;
            return sended;
        }

        return sended;
    }

    [WebMethod]
    public bool SendMeetingInvite(string body, string subject, string toAddress,string date)
    {
        bool sended = false;
        string[] emailList;


        try
        {

            string from = (date + " 10:00:00").Replace(":", "").Replace("/", "").Replace("-", "").Replace(" ", "");
            string to = (date + " 10:30:00").Replace(":", "").Replace("/", "").Replace("-", "").Replace(" ", "");
            DateTime fromdt = DateTime.ParseExact(from, "yyyyMMddHHmmss", null);
            DateTime todt = DateTime.ParseExact(to, "yyyyMMddHHmmss", null);

            string location = "";

            DateTime stamp = fromdt;
            emailList = new string[toAddress.Split(';').Length];
            emailList = toAddress.Split(';');




                using (SqlConnection cn = new SqlConnection(
    System.Configuration.ConfigurationManager.ConnectionStrings["DRCConnectioString2"].ConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '3'";
                    dt.Load(cmd.ExecuteReader());

                    string server = "";
                    string username = "";
                    string password = "";
                    string fromA = "";
                    string port = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        username = row["USERNAME"].ToString();
                        fromA = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();

                    }

                        sended = PAMC.Utilities.Email.Sender.SendMeetingInviteViaSMTP(server, username, password, fromA, subject, body,toAddress,fromdt,todt, null);
                    
                }


            
        }
        catch (Exception e)
        {
            var msg = e.Message;
            return sended;
        }

        return sended;
    }

    [WebMethod]

    public bool SendAuthorization(int headerid)
    {
        bool success = false ;

        try
        {
            using (SqlConnection cn = new SqlConnection("Server=192.168.16.13;Database=PamcPortal;User Id=pamcportal;Password=#Portal@Pamc;"))
            {
                SqlCommand cmd = new SqlCommand();
                DataTable dt = new DataTable();
                DataTable tbl = new DataTable();
                DataTable dtbl = new DataTable();
                DataTable data = new DataTable();
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();

                }

                cmd.Connection = cn;

                int _authId = 0;
                _authId = headerid;
                string username = "";
                string provid = "";
                string family = "";
                string hp = "";
                int filecount = 0;
                string submit = "";
                string[] emails;
                string text = "";
                string mails = "";




                    DataTable datatbl = new DataTable();
                    SqlCommand cmd1 = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '3'";
                datatbl.Load(cmd.ExecuteReader());

                    string server = "";
                    string usernameL = "";
                    string password = "";
                    string from = "";
                    string port = "";

                    foreach (DataRow row in datatbl.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                    usernameL = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();
                    }


                SmtpClient client = new SmtpClient(server);
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(usernameL, password);
                client.Port = Convert.ToInt32(port);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);



                cmd.CommandText = "SELECT * FROM AuthsHeaderFile WHERE HeadID = "+_authId.ToString()+"";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow row in dt.Rows)
                {
                    provid = row["ProvId"].ToString();
                    family = row["MembId"].ToString();
                    username = row["Username"].ToString();
                    hp = row["HPCode"].ToString();
                    submit = row["CreateDate"].ToString();
                    text = row["Text"].ToString();
                }

                cmd.CommandText = "SELECT * FROM Auth_Email_Log WHERE HPCODE = '"+hp+"' AND DATESENT = '"+DateTime.Now.ToString("yyyy/MM/dd")+"' ORDER BY COUNTER ASC";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    tbl.Load(dr);
                }
                int counter = 0;
                string to = "";
                if (tbl.Rows.Count > 0)
                {
                    emails = new string[1];
                    emails[0] = tbl.Rows[0]["EMAIL"].ToString();
                    to = tbl.Rows[0]["EMAIL"].ToString();
                    mail.To.Add(emails[0]);
                    counter = Convert.ToInt32(tbl.Rows[0]["COUNTER"].ToString()) + 1;
                    cmd.CommandText = "UPDATE Auth_Email_Log SET COUNTER = "+counter.ToString()+" WHERE HPCODE = '"+hp+"' AND EMAIL = '"+emails[0]+"' AND DATESENT = '"+DateTime.Now.ToString("yyyy/MM/dd")+"'";
                    cmd.ExecuteNonQuery();

                }
                else
                {
                    cmd.CommandText = "SELECT EMAILADDRESSES FROM Internal_Auth_Distribution WHERE HPCODE = '"+hp+"'";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtbl.Load(dr);
                    }
                    foreach (DataRow row in dtbl.Rows)
                    {
                        mails = row["EMAILADDRESSES"].ToString();

                    }

                    emails = new string[mails.Split(';').Length];
                    emails = mails.Split(';');
                    foreach (string email in emails)
                    {
                        cmd.CommandText = "INSERT INTO Auth_Email_Log (HPCODE,EMAIL,DATESENT,COUNTER,CREATEDATE,CREATEBY,LASTCHANGEDATE,LASTCHANGEBY) VALUES " +
                            "('"+hp+"','"+email+"','"+DateTime.Now.ToString("yyyy/MM/dd")+"',0,'"+DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")+"'," +
                            "999,'"+DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")+"',999)";
                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = "UPDATE Auth_Email_Log SET COUNTER = 1 WHERE HPCODE = '"+hp+"' AND EMAIL = '"+emails[0]+"' " +
                        "AND DATESENT = '"+DateTime.Now.ToString("yyyy/MM/dd")+"'";
                    cmd.ExecuteNonQuery();
                    to = emails[0];
                    
                }

                cmd.CommandText = "SELECT * FROM AuthDetailFile WHERE HeadID =" + _authId.ToString();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    data.Load(dr);
                }

                DirectoryInfo d = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath+ "files\\");
                foreach (FileInfo item in d.GetFiles())
                {
                    FileInfo f = new FileInfo(item.FullName);
                    f.Delete();
                }

                List<FileInfo> ff = new List<FileInfo>();

                foreach (DataRow row in data.Rows)
                {
                    using(var ms = new MemoryStream())
                    {
                        var filebytes = (byte[])row["ByteArray"];
                        var FileName = row["FileName"].ToString();
                        FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "files\\" +FileName, FileMode.Create);
                        MemoryStream m = new MemoryStream(filebytes);
                        m.WriteTo(fs);

                        FileInfo files = new FileInfo(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "files\\" + FileName);
                        if (files.Exists)
                        {
                            ff.Add(files);
                        }
                        fs.Flush();
                        fs.Dispose();
                        fs.Close();
                        ms.Flush();
                        ms.Dispose();
                        ms.Close();
                        m.Flush();
                        m.Dispose();
                        m.Close();
                    }

                }
                filecount = data.Rows.Count;

                string subject = "Authorization Submission";
                string body = "Good day, \n\n" +
                    "An Authorization was uploaded via the provider portal on "+submit+".\n\n" +
                    "Details of Auth: \n" +
                    "REFERENCE: #" + _authId.ToString()+" \n" +
                    "USERNAME: "+username+" \n" +
                    "PROVID: "+provid+" \n" +
                    "FAMILY NO: "+family.Remove(0, 1)+" \n" +
                    "HPCODE: "+hp+" \n" +
                    "NO. OF FILES: "+ filecount.ToString()+ "\n\n" +
                    "Additional Notes: \n\n "+text+"\n\n\n" +
                    "Regards.";

                //client.Send(mail);

                cmd.CommandText = "SELECT * FROM DRC..EDI_MAILSERVERS WHERE ROWID = '3'";
                dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                server = "";
                 username = "";
                 password = "";
                 from = "";
                 port = "";

                foreach (DataRow row in dt.Rows)
                {
                    server = row["SERVERADDRESS"].ToString();
                    password = row["PASSWORD"].ToString();
                    username = row["USERNAME"].ToString();
                    from = row["MAILFROM"].ToString();
                    port = row["OUTGOINGPORT"].ToString();

                }

                success = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, to, ff); 



            }
        }
        catch (Exception e)
        {

            success = false;
            var msg = e.Message;
        }


        return success;
    }

}
