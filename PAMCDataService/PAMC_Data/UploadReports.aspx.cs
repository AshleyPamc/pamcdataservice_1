﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class UploadReports : System.Web.UI.Page
{
    private List<FileUpload> files = new List<FileUpload>();
    DataTable tbl = new DataTable();
    public static FileUpload f = new FileUpload();
    public static HttpPostedFile ff;


    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                tbl = new DataTable();
                cmd.CommandText = "SELECT FileName,LongDescription,ShortDescription,LastUpdateDate FROM ReportUploadFile";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    tbl.Load(dr);
                }
                GridView1.Visible = true;
                GridView1.DataSource = tbl;
                GridView1.DataBind();
                memoField.Text = "";
                Panel1.Visible = true;
                Panel2.Visible = false;

                //foreach (DataRow row in tbl.Rows)
                //{
                //    FileUpload item = new FileUpload();

                //    item.changeDate = row["LastUpdateDate"].ToString();
                //    item.longDesc = row["LongDescription"].ToString();
                //    item.shortDesc = row["ShortDescription"].ToString();
                //    item.fileName = row["FileName"].ToString();

                //    files.Add(item);
                //}


            }
        }
        catch (Exception ex)
        {

            var mes = ex.Message;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;
        string _shortDesc = "";
        string _longDesc = "";
        string _memo1 = "";
        string _memo2 = "";
        FileUpload file = new FileUpload();

        bool shortEntered = false;
        bool longEntered = false;
        bool fileEntered = false;


        _shortDesc = shortDesc.Text;
        _longDesc = longDesc.Text;
        _memo1 = memo1.Text.Replace("'", ""); ;
        _memo2 = memo2.Text.Replace("'", ""); ;

        try
        {

            file.memo1 = _memo1;
            file.memo2 = _memo2;
            if ((_shortDesc != "") && (_shortDesc != null))
            {
                file.shortDesc = _shortDesc;
                shortEntered = true;
            }
            else
            {
                Type csType = this.GetType();
                ClientScriptManager cs = Page.ClientScript;
                string sctext = "alert('A short description for the file is required!')";
                cs.RegisterStartupScript(csType, "PopupScript", sctext, true);

            }
            if ((_longDesc != "") && (_longDesc != null))
            {
                file.longDesc = _longDesc;
                longEntered = true;
            }
            else
            {
                Type csType = this.GetType();
                ClientScriptManager cs = Page.ClientScript;
                string sctext = "alert('A long description for the file is required!')";
                cs.RegisterStartupScript(csType, "PopupScript", sctext, true);
            }

            if (FileUpload1.HasFile)
            {
                var selectedFile = FileUpload1.PostedFile;
                file.fileName = selectedFile.FileName;
                fileEntered = true;
            }
            else
            {
                Type csType = this.GetType();
                ClientScriptManager cs = Page.ClientScript;
                string sctext = "alert('Please Select a file!')";
                cs.RegisterStartupScript(csType, "PopupScript", sctext, true);
            }

            if ((shortEntered) && (longEntered) && (fileEntered))
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@name", file.fileName));

                    cmd.CommandText = "SELECT * FROM ReportUploadFile WHERE FileName = @name";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        Type csType = this.GetType();
                        ClientScriptManager cs = Page.ClientScript;
                        string sctext = "alert('This document already exists, please fill in the necessary detail, and use the update button in the coloum below by the selected document to update the document.')";
                        cs.RegisterStartupScript(csType, "PopupScript", sctext, true);
                    }
                    else
                    {
                        InsertNewDocument(file, cmd);

                    }


                }

            }
        }
        catch (Exception ex)
        {

            var msg = ex.Message;
        }


    }

    private void InsertNewDocument(FileUpload file, SqlCommand cmd)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;
        var selectedFile = FileUpload1.PostedFile;
        using (var ms = new MemoryStream())
        {
            selectedFile.InputStream.CopyTo(ms);
            byte[] filebytes = ms.ToArray();
            cmd.Parameters.Add(new SqlParameter("@long", file.longDesc));
            cmd.Parameters.Add(new SqlParameter("@short", file.shortDesc));
            cmd.Parameters.Add(new SqlParameter("@memo1", file.memo1));
            cmd.Parameters.Add(new SqlParameter("@memo2", file.memo2));
            cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
            cmd.Parameters.Add("@filebytes", SqlDbType.VarBinary, filebytes.Length).Value = filebytes;
            cmd.Parameters.Add(new SqlParameter("@type", ".rpt"));

            cmd.CommandText = "INSERT INTO ReportUploadFile (FileName,FileType,LongDescription,ShortDescription,Memo1,Memo2,FileBytes,CreateDate,LastUpdateDate)" +
                " VALUES (@name,@type,@long,@short,@memo1,@memo2,@filebytes,@date,@date)";
            cmd.ExecuteNonQuery();


            tbl = new DataTable();
            cmd.CommandText = "SELECT FileName,LongDescription,ShortDescription,LastUpdateDate FROM ReportUploadFile";
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                tbl.Load(dr);
            }
            GridView1.Visible = true;
            GridView1.DataSource = tbl;
            GridView1.DataBind();
            shortDesc.Text = "";
            longDesc.Text = "";

        }
    }

    private void UpdateExistingRow(FileUpload file, SqlConnection cn, SqlCommand cmd, string name)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;
        cmd.Connection = cn;
        var selectedFile = FileUpload1.PostedFile;
        try
        {
            if (name == selectedFile.FileName)
            {
                using (var ms = new MemoryStream())
                {

                    selectedFile.InputStream.CopyTo(ms);
                    byte[] filebytes = ms.ToArray();

                    cmd.Parameters.Add(new SqlParameter("@memo1", file.memo1));
                    cmd.Parameters.Add(new SqlParameter("@memo2", file.memo2));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add("@filebytes", SqlDbType.VarBinary, filebytes.Length).Value = filebytes;
                    cmd.Parameters.Add(new SqlParameter("@long", file.longDesc));
                    cmd.Parameters.Add(new SqlParameter("@short", file.shortDesc));

                    cmd.CommandText = "UPDATE ReportUploadFile SET LongDescription = @long, ShortDescription = @short,Memo1=@memo1, Memo2 = @memo2, FileBytes = @filebytes ,LastUpdateDate = @date" +
                        " WHERE FileName = @name";
                    cmd.ExecuteNonQuery();
                    tbl = new DataTable();
                    cmd.CommandText = "SELECT FileName,LongDescription,ShortDescription,LastUpdateDate FROM ReportUploadFile";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tbl.Load(dr);
                    }
                    GridView1.Visible = true;
                    GridView1.DataSource = tbl;
                    GridView1.DataBind();
                    shortDesc.Text = "";
                    longDesc.Text = "";
                }
            }
            else
            {
                Type csType = this.GetType();
                ClientScriptManager cs = Page.ClientScript;
                string sctext = "alert('You have selected the incorrect file to update, please make sure you have uploaded the correct file and selected the correct row to be updated!')";
                cs.RegisterStartupScript(csType, "PopupScript", sctext, true);
            }
            
        }
        catch (Exception e)
        {

            throw;
        }
    }

    //public void updateGrid()
    //{
    //    try
    //    {
    //        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
    //        {
    //            if (cn.State != ConnectionState.Open)
    //            {
    //                cn.Open();
    //            }

    //            SqlCommand cmd = new SqlCommand();
    //            cmd.Connection = cn;
    //            tbl = new DataTable();
    //            cmd.CommandText = "SELECT FileName,LongDescription,ShortDescription,LastUpdateDate FROM ReportUploadFile";
    //            using (SqlDataReader dr = cmd.ExecuteReader())
    //            {
    //                tbl.Load(dr);
    //            }
    //            //GridView1.Visible = true;
    //            GridView1.DataSource = tbl;
    //            GridView1.DataBind();


    //            //foreach (DataRow row in tbl.Rows)
    //            //{
    //            //    FileUpload item = new FileUpload();

    //            //    item.changeDate = row["LastUpdateDate"].ToString();
    //            //    item.longDesc = row["LongDescription"].ToString();
    //            //    item.shortDesc = row["ShortDescription"].ToString();
    //            //    item.fileName = row["FileName"].ToString();

    //            //    files.Add(item);
    //            //}


    //        }
    //    }
    //    catch (Exception e)
    //    {

    //        throw;
    //    }
    //}

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;
        string fileName = "";
        fileName = tbl.Rows[e.NewSelectedIndex]["FileName"].ToString();



        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                DataTable dt = new DataTable();

                cmd.Parameters.Add(new SqlParameter("@name", fileName));
                cmd.CommandText = "SELECT FileBytes FROM ReportUploadFile WHERE FileName = @name";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                // byte[] filebytes;

                foreach (DataRow row in dt.Rows)
                {

                    var filebytes = (byte[])row["FileBytes"];
                    Response.Clear();
                    Response.ContentType = "application/force-download";
                    Response.AddHeader("content-disposition", "attachment;    filename=" + fileName);
                    Response.BinaryWrite(filebytes);
                    //Response.End();


                }


            }

        }
        catch (Exception ex)
        {

            var msg = ex.Message;
        }
        //MemoryStream memoryStream = new MemoryStream();
        //TextWriter textWriter = new StreamWriter(memoryStream);
        //textWriter.WriteLine("Something");
        //textWriter.Flush(); // added this line
        //byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
        //memoryStream.Close();

        //Response.Clear();
        //Response.ContentType = "application/force-download";
        //Response.AddHeader("content-disposition", "attachment;    filename=name_you_file.xls");
        //Response.BinaryWrite(bytesInStream);
        //Response.End();


    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;

        GridView1.PageIndex = e.NewPageIndex;
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
        {
            if (cn.State != System.Data.ConnectionState.Open)
            {
                cn.Open();
            }

            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            cmd.Connection = cn;


            tbl = new DataTable();
            cmd.CommandText = "SELECT FileName,LongDescription,ShortDescription,LastUpdateDate FROM ReportUploadFile";
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                tbl.Load(dr);
            }
            GridView1.Visible = true;
            GridView1.DataSource = tbl;
            GridView1.DataBind();





        }

    }

    [WebMethod]
    public static void updateFile()
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
            {
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                DataTable dt = new DataTable();
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@name", f.fileName));

                using (var ms = new MemoryStream())
                {

                    ff.InputStream.CopyTo(ms);
                    byte[] filebytes = ms.ToArray();

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add("@filebytes", SqlDbType.VarBinary, filebytes.Length).Value = filebytes;
                    cmd.Parameters.Add(new SqlParameter("@long", f.longDesc));
                    cmd.Parameters.Add(new SqlParameter("@short", f.shortDesc));

                    cmd.CommandText = "UPDATE ReportUploadFile SET LongDescription = @long, ShortDescription = @short, FileBytes = @filebytes ,LastUpdateDate = @date" +
                        " WHERE FileName = @name";
                    cmd.ExecuteNonQuery();

                    //  u.updategrid();



                }
                //  UploadReports u = new UploadReports();
                //  u.updateGrid();

                // Page_Load(object sender, EventArgs e);
            }

        }
        catch (Exception e)
        {
            var msg = e.Message;
            throw;
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;

        int page = GridView1.PageIndex;

        if (e.CommandName.CompareTo("DeleteDocument") == 0)
        {
            string fileName = "";


            fileName = tbl.Rows[((page * 10) + Convert.ToInt32(e.CommandArgument))]["FileName"].ToString();



            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@name", fileName));
                    cmd.CommandText = "DELETE FROM ReportUploadFile WHERE FileName = @name";
                    cmd.ExecuteNonQuery();
                    tbl = new DataTable();
                    cmd.CommandText = "SELECT FileName,LongDescription,ShortDescription,LastUpdateDate FROM ReportUploadFile";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tbl.Load(dr);
                    }
                    GridView1.Visible = true;
                    GridView1.DataSource = tbl;
                    GridView1.DataBind();



                }

            }
            catch (Exception ex)
            {

                var msg = ex.Message;
            }
        }
        
        if (e.CommandName.CompareTo("Download") == 0)
        {
            string fileName = "";
            fileName = tbl.Rows[((page * 10) + Convert.ToInt32(e.CommandArgument))]["FileName"].ToString();



            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@name", fileName));
                    cmd.CommandText = "SELECT FileBytes FROM ReportUploadFile WHERE FileName = @name";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }
                    // byte[] filebytes;

                    foreach (DataRow row in dt.Rows)
                    {

                        var filebytes = (byte[])row["FileBytes"];
                        Response.Clear();
                        Response.ContentType = "application/force-download";
                        Response.AddHeader("content-disposition", "attachment;    filename=" + fileName);
                        Response.BinaryWrite(filebytes);
                        //Response.End();


                    }


                }

            }
            catch (Exception ex)
            {

                var msg = ex.Message;
            }
        }

        if (e.CommandName.CompareTo("UpdateDocument") == 0)
        {
            string fileName = tbl.Rows[((page * 10) + Convert.ToInt32(e.CommandArgument))]["FileName"].ToString();
            string _shortDesc = "";
            string _longDesc = "";
            string _memo1 = "";
            string _memo2 = "";
            FileUpload file = new FileUpload();

            bool shortEntered = false;
            bool longEntered = false;
            bool fileEntered = false;


            _shortDesc = shortDesc.Text;
            _longDesc = longDesc.Text;
            _memo1 = memo1.Text.Replace("'", ""); ;
            _memo2 = memo2.Text.Replace("'", ""); ;

            try
            {

                file.memo1 = _memo1;
                file.memo2 = _memo2;
                if ((_shortDesc != "") && (_shortDesc != null))
                {
                    file.shortDesc = _shortDesc;
                    shortEntered = true;
                }
                else
                {
                    Type csType = this.GetType();
                    ClientScriptManager cs = Page.ClientScript;
                    string sctext = "alert('A short description for the file is required!')";
                    cs.RegisterStartupScript(csType, "PopupScript", sctext, true);

                }
                if ((_longDesc != "") && (_longDesc != null))
                {
                    file.longDesc = _longDesc;
                    longEntered = true;
                }
                else
                {
                    Type csType = this.GetType();
                    ClientScriptManager cs = Page.ClientScript;
                    string sctext = "alert('A long description for the file is required!')";
                    cs.RegisterStartupScript(csType, "PopupScript", sctext, true);
                }

                if (FileUpload1.HasFile)
                {
                    var selectedFile = FileUpload1.PostedFile;
                    file.fileName = selectedFile.FileName;
                    fileEntered = true;
                }
                else
                {
                    Type csType = this.GetType();
                    ClientScriptManager cs = Page.ClientScript;
                    string sctext = "alert('Please Select a file!')";
                    cs.RegisterStartupScript(csType, "PopupScript", sctext, true);
                }

               

                if ((shortEntered) && (longEntered) && (fileEntered))
                {
                    using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
                    {
                        if (cn.State != System.Data.ConnectionState.Open)
                        {
                            cn.Open();
                        }

                        SqlCommand cmd = new SqlCommand();
                        DataTable dt = new DataTable();
                        cmd.Connection = cn;

                        cmd.Parameters.Add(new SqlParameter("@name", fileName));
                        UpdateExistingRow(file, cn, cmd, fileName);
                    }

                }
            }
            catch (Exception ex)
            {

                var msg = ex.Message;
            }
        }

        if (e.CommandName.CompareTo("Memo1") == 0)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
                {
                    string fileName = tbl.Rows[((page * 10) + Convert.ToInt32(e.CommandArgument))]["FileName"].ToString();
                    SqlCommand cmd = new SqlCommand();

                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@name", fileName));

                    cmd.CommandText = "SELECT Memo1 FROM ReportUploadFile WHERE FileName = @name";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    string memo = dt.Rows[0][0].ToString();

                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    memoField.Text = memo;

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        if (e.CommandName.CompareTo("Memo2") == 0)
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DRCConnectioString3"].ToString()))
            {
                string fileName = tbl.Rows[((page * 10) + Convert.ToInt32(e.CommandArgument))]["FileName"].ToString();
                SqlCommand cmd = new SqlCommand();

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@name", fileName));

                cmd.CommandText = "SELECT Memo2 FROM ReportUploadFile WHERE FileName = @name";

                DataTable dt = new DataTable();

                dt.Load(cmd.ExecuteReader());

                string memo = dt.Rows[0][0].ToString();
                Panel1.Visible = false;
                Panel2.Visible = true;
                memoField.Text = memo;

            }
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        memoField.Text = "";
        Panel1.Visible = true;
        Panel2.Visible = false;
    }
}