﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadReports.aspx.cs" Inherits="UploadReports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <title></title>
    <script type="text/javascript" lang="javascript">
        function ConfirmApproval(objMsg) {
            if (confirm(objMsg)) {
                PageMethods.updateFile(OnSuccess, OnFailure);
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
            }
            else {
                return false;
            }
            function OnSuccess(result) {
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                window.location.href = "UploadReports.aspx";
                alert("UPDATED!!");


            }
            function OnFailure(error) {
                alert("Oops!! An Error Ocurred " + error);
            }

        }
    </script>
</head>
<body style="background: #DDDDDD">
    <form id="form1" runat="server">
        <asp:ScriptManager ID='ScriptManager1' runat='server' EnablePageMethods='true' />
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6 text-center">
                    <br />
                    <h3 style="text-decoration: underline; font-weight: 800">UPLOAD AND EDIT REPORTS</h3>
                    <br />
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                <asp:Panel ID="Panel1" runat="server">
                <div class="row" style="border: solid; background-color: #E1F1F6; border-width: thin; border-radius: 2px; border-color: #89CBDF; box-shadow: 5px 10px #9A9A9A">
                            <div class="col-lg-12">
                                <div class="row">
                                <div class="col-4">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label><b>Select a File</b></label>
                                        </div>
                                        <div class="col-md-8" >
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <label><b>Short Description</b></label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="shortDesc" TextMode="multiline" Columns="50" Rows="2" runat="server" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label><b>Long Description</b></label>
                                        </div>
                                        <div class="col-md-8" >
                                            <asp:TextBox ID="longDesc" TextMode="multiline" Columns="50" Rows="5" runat="server" />
                                        </div>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-8">
                                <br />
                                    
                                <div class="row">
                                    <div class="col-md-4" style="text-align: right">
                                        <label><b>Memo1</b></label>
                                    </div>
                                    <div class="col-md-8" style="text-align: left">
                                        <asp:TextBox ID="memo1" TextMode="multiline" Columns="60" Rows="9" runat="server" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-4" style="text-align: right">
                                        <label><b>Memo2</b></label>
                                    </div>
                                    <div class="col-md-8" style="text-align: left">
                                        <asp:TextBox ID="memo2" TextMode="multiline" Columns="60" Rows="9" runat="server" />
                                    </div>
                                </div>
                                <br />
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5" style="text-align: right">
                                    </div>
                                    <div class="col-md-7" style="text-align: left">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>

                <br />

                <br />
                <div class="row">

                    <div class="col-12" style="text-align: left">

                        <asp:GridView ID="GridView1" runat="server" BackColor="#EEEEEE" BorderStyle="Ridge" BorderWidth="2px" CellPadding="5" CellSpacing="5" HorizontalAlign="Center" AllowPaging="True" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand">
                            <Columns>

                                <asp:ButtonField ButtonType="Button" Text="Update" CommandName="UpdateDocument" />
                                <asp:ButtonField ButtonType="Button" Text="Delete" CommandName="DeleteDocument" />
                                <asp:ButtonField ButtonType="Button" Text="Download" CommandName="Download" />
                                <asp:ButtonField ButtonType="Button" Text="Memo1 view" CommandName="Memo1" />
                                <asp:ButtonField ButtonType="Button" Text="Memo2 view" CommandName="Memo2" />

                            </Columns>
                        </asp:GridView>

                    </div>

                </div>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server">
                <label>Memo Field:</label><br />
                <asp:Label ID="memoField" runat="server" Text=""></asp:Label>
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="OK" OnClick="Button2_Click" />
            </asp:Panel>
                </div>

            </div>
        </div>
    </form>

    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">WARNING!!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>please enter a short description.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</body>
</html>
